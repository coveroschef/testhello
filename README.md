testhello Cookbook
====================
Dummy test cookbook for playing with Chef. Generates a file /tmp/hello_world.txt
with a short text message in it.

Requirements
------------
Pre-installed Linux server that has Chef Client installed and configured to talk
to the Chef host. User should have .chef directory set up with proper knife.rb 
settings to work with the cookbook and /etc/chef setup with client.rb for chef-client.


Attributes
----------
TODO: List you cookbook attributes here.

e.g.
#### testhello::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['testhello']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### testhello::default

Run manually with 

```chef-client -o testhello```

or 
Just include `testhello` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[testhello]"
  ]
}
```

License and Authors
-------------------
Authors: Rich Mills
