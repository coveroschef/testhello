name             'testhello'
maintainer       'Coveros'
maintainer_email 'rich.mills@coveros.com'
license          'All rights reserved'
description      'Test cookbook for playing with and learning chef'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))

#
version          '0.4.0'

# v0.4.0 - initial import after making generic for learning
# v0.3.0 - previous client specific version while working with AOL

