#
# Cookbook Name:: testhello
# Recipe:: default
#
# No copyright. Do whatever you want with this.
#
# Dummy test recipe to test Chef stuff

# Local temp variable with the node attribute for the name we will say "Hello" to
hello_name = node[:testhello][:name]

# Create hello_world.txt file
file "create hello_world.txt" do
  path "/tmp/hello_world.txt"
  content "Hello, #{hello_name}!"
  action :create
end

# Log a message with 'helper code' during compile phase
Chef::Log.info("Helper code: Hello, #{hello_name}")

# Log a message with a resource during node convergence phase
log "log message example" do
  message "Log resource: Hello, #{hello_name}"
  level :info
end